// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
//
// This file is part of SocketServer.
//
// SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
// SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

import PackageDescription

#if os(Linux)
let package = Package(
    name: "SocketServer",
    platforms: [SupportedPlatform.macOS(.v11)],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "SocketServer",
            targets: ["SocketServer"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "Socket", url: "git@gitlab.com:marshall-software-games/epikt/forked-dependencies/BlueSocket.git", from: "2.0.2"),
        .package(name: "SSLService", url: "git@gitlab.com:marshall-software-games/epikt/forked-dependencies/bluesslservice.git", from: "3.0.0-fork"),
        // .package(name: "SSLService", path: "../bluesslservice"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "SocketServer",
            dependencies: ["Socket", "SSLService"]
        ),
        .testTarget(
            name: "SocketServerTests",
            dependencies: ["SocketServer"]
        )
    ]
)

#else
fatalError("Unsupported OS")
#endif
