//
//  SSLConfiguration.swift
//  SocketServer
//
//  Created by Justin Marshall on 12/23/21.
//  Copyright © 2019 Justin Marshall. All rights reserved.
//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

#if os(Linux)
import SSLService

public struct SSLConfiguration {
   public let caCertificateDirectory: String?
   public let certificateFilePath: String
   public let keyFilePath: String

   public init(caCertificateDirectory: String?, certificateFilePath: String, keyFilePath: String) {
       self.caCertificateDirectory = caCertificateDirectory
       self.certificateFilePath = certificateFilePath
       self.keyFilePath = keyFilePath
   }

   internal var configuration: SSLService.Configuration {
        SSLService.Configuration(
                        withCACertificateDirectory: caCertificateDirectory,
                        usingCertificateFile: certificateFilePath,
                        withKeyFile: keyFilePath
        )
   }
}
#endif
