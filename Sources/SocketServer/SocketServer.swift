//
//  SocketServer.swift
//  SocketServer
//
//  Created by Justin Marshall on 6/13/19.
//  Copyright © 2019 Justin Marshall. All rights reserved.
//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

#if SWIFT_PACKAGE
import Foundation
#endif
import Socket

#if os(Linux)
import SSLService
import OpenSSL
#endif

public final class SocketServer {
    private enum ConnectionType {
        case single(responseOnConnect: ResponseOnConnect)
        case persistent(responseOnConnect: ResponseOnConnect, responseForInput: ResponseForInput, unexpectedClose: UnexpectedClose)
    }

    private let port: Int
    private let bufferSize: Int

    private let connectionType: ConnectionType

    public typealias ResponseOnConnect = (_ request: String?,_ socket: Connection, _ x509Certificate: X509Certificate?) -> (text: String, binary: Data?)
    public typealias ResponseForInput = (_ userInput: String, _ socket: Connection) -> String
    public typealias UnexpectedClose = (_ socket: Connection) -> Void

    #if os(Linux)
    private let sslConfiguration: SSLConfiguration?
    #endif

    private let socketLockQueue = DispatchQueue(label: "com.socketserver.socketLockQueue")

    private var certifiedConnections: [Socket: X509Certificate] = [:]
    private var activeConnections: Set<Connection> = []
    private var connectionListenerSocket: Socket?

    private var _continueRunning = true
    private var continueRunning: Bool {
        set {
            socketLockQueue.sync {
                self._continueRunning = newValue
            }
        }
        get {
            return socketLockQueue.sync {
                self._continueRunning
            }
        }
    }

    #if os(Linux)
    public init(port: Int, sslConfiguration: SSLConfiguration?, bufferSize: Int = 4_096, responseOnConnect: @escaping ResponseOnConnect) {
        self.port = port
        self.connectionType = .single(responseOnConnect: responseOnConnect)
        self.sslConfiguration = sslConfiguration
        self.bufferSize = bufferSize
    }

    public init(port: Int, sslConfiguration: SSLConfiguration?, bufferSize: Int = 4_096, responseOnConnect: @escaping ResponseOnConnect, responseForInput: @escaping ResponseForInput, unexpectedClose: @escaping UnexpectedClose) {
        self.port = port
        self.connectionType = .persistent(responseOnConnect: responseOnConnect, responseForInput: responseForInput, unexpectedClose: unexpectedClose)
        self.sslConfiguration = sslConfiguration
        self.bufferSize = bufferSize
    }
    #else
    public init(port: Int, bufferSize: Int = 4_096, responseOnConnect: @escaping ResponseOnConnect) {
        self.port = port
        self.connectionType = .single(responseOnConnect: responseOnConnect)
        self.sslConfiguration = nil
        self.bufferSize = bufferSize
    }

    public init(port: Int, bufferSize: Int = 4_096, responseOnConnect: @escaping ResponseOnConnect, responseForInput: @escaping ResponseForInput, unexpectedClose: @escaping UnexpectedClose) {
        self.port = port
        self.connectionType = .persistent(responseOnConnect: responseOnConnect, responseForInput: responseForInput, unexpectedClose: unexpectedClose)
        self.sslConfiguration = nil
        self.bufferSize = bufferSize
    }
    #endif

    deinit {
        activeConnections.forEach { connection in
            connection.close()
        }

        connectionListenerSocket?.close()
    }

    private var onListen: (() -> Void)?
    @discardableResult
    public func onListen(notifier: @escaping () -> Void) -> Self {
        self.onListen = notifier
        return self
    }

    private var onAccept: ((_ sourceHost: String) -> Void)?
    @discardableResult
    public func onAccept(notifier: @escaping (_ sourceHost: String) -> Void) -> Self {
        self.onAccept = notifier
        return self
    }

    private var onError: ((_ error: Error) -> Void)?
    @discardableResult
    public func onError(notifier: @escaping (_ error: Error) -> Void) -> Self {
        self.onError = notifier
        return self
    }

    private var onUnrecoverableError: ((_ error: Error) -> Void)?
    @discardableResult
    public func onUnrecoverableError(notifier: @escaping (_ error: Error) -> Void) -> Self {
        self.onUnrecoverableError = notifier
        return self
    }

    private var onUserInput: ((_ sourceHost: String, _ userInput: String) -> Void)?
    @discardableResult
    public func onUserInput(notifier: @escaping (_ sourceHost: String, _ userInput: String) -> Void) -> Self {
        self.onUserInput = notifier
        return self
    }

    private var onClose: ((_ sourceHost: String) -> Void)?
    @discardableResult
    public func onClose(notifier: @escaping (_ sourceHost: String) -> Void) -> Self {
        self.onClose = notifier
        return self
    }

    public func shutdown() {
        continueRunning = false

        // Close all open sockets
        activeConnections.forEach { connection in
            socketLockQueue.sync { [unowned self] in
                self.activeConnections.remove(connection)
                connection.close()
            }
        }

        DispatchQueue.main.async {
            exit(EXIT_SUCCESS)
        }
    }

    public func boot() {
        let queue = DispatchQueue.global()
        queue.async { [unowned self] in
            do {
                // Create an IPV4 socket
                let listeningSocket = try Socket.create(family: .inet)
                self.connectionListenerSocket = listeningSocket

                #if os(Linux)
                if let configuration = sslConfiguration?.configuration {
                    let sslService = try SSLService(usingConfiguration: configuration)
                    sslService?.skipVerification = true
                    sslService?.verifyCallback = { [weak self] service, socket in
                        let x509Certificate = X509Certificate(from: service.cSSL)
                        self?.certifiedConnections[socket] = x509Certificate

                        // current use case makes use of certificate validation at the application layer
                        // so we just tell the SSL layer to accept it
                        return (true, nil)
                    }
                    listeningSocket.delegate = sslService
                }
                #endif

                try listeningSocket.listen(on: self.port)
                onListen?()
                acceptClientConnection(on: listeningSocket)
            } catch let error as Socket.Error {
                handle(socketError: error)
            } catch let error as SSLError {
                // ssl lib throws this when the given path to the certificate cannot be found, immediately on program launch
                // its definition is as follows:
                // #define ENOENT   2 /* No such file or directory */
                if error.errCode == ENOENT {
                    onUnrecoverableError?(error)
                } else {
                    onError?(error)
                }
            } catch {
                onError?(error)
            }
        }
        dispatchMain()
    }

    private func handle(socketError: Socket.Error) {
        guard let connectionListenerSocket = connectionListenerSocket else {
            onUnrecoverableError?(socketError)
            return
        }
        /**
        151584876 is the error code that BlueSSLService spits out when you try giving
        it an SSL private or public key that does not have a "start line".

        For example:
        - If you point both the private key and public key to the same file
        - Mix up the private and public key
        - Point to a non ".pem" file

        Calling `onError?` in this scenario was causing an infinite loop in a consuming
        application because it (rightly so) was not shutting itself down, which then
        allowed this code to continue on into what is now in the else block, ultimately
        causing infinite errors to be logged.

        After quite some time digging through OpenSSL documentation and C source (!),
        I found some promising code.

        One of which is a constant, PEM_R_NO_START_LINE, which sounds like it'd be what
        we need here, but as of time of writing, it is instead:

        # define PEM_R_NO_START_LINE                              108

        ... which appears further defined like so:

        crypto/pem/pem_err.c:    {ERR_PACK(ERR_LIB_PEM, 0, PEM_R_NO_START_LINE), "no start line"},

        Unfortunately, that 108 is not the error that BlueSSLService is ultimately
        spitting out (see above). How to convert between the two?

        Continuing to poke into the OpenSSL source, I eventually found the following code to
        convert error codes:

        if (ERR_GET_REASON(ERR_peek_error()) == PEM_R_NO_START_LINE)

        The Swift code in this file is able to see and use both ERR_peek_error() and
        PEM_R_NO_START_LINE, but cannot see ERR_GET_REASON, despite it being in the same
        file as the other two.

        # define ERR_GET_REASON(l)       (int)( (l)         & 0xFFFL)

        Turns out that, according to Swift documentation and compiler, function-like
        macros are not callable from Swift:

        Building for debugging...
        SocketServer/Sources/SocketServer/SocketServer.swift:237:23: error: cannot find 'ERR_GET_REASON' in scope
        print(ERR_GET_REASON(ERR_peek_error()) == PEM_R_NO_START_LINE)
                      ^~~~~~~~~~~~~~
        /usr/include/openssl/err.h:144:10: note: macro 'ERR_GET_REASON' unavailable: function like macros not supported
        # define ERR_GET_REASON(l)       (int)( (l)         & 0xFFFL)
                         ^
        error: fatalError

        I then tried to workaround this via a modulemap, exposing this C wrapper code in a header:

        inline int err_get_reason(int l) {
            return ERR_GET_REASON(l);
        }

        ... but it turns out that getting Swift to see something like that requires a Swift
        package to wrap it, which is an unreasonable level of effort just to use an
        official constant.

        Another option would be to submit a patch to the Swift OpenSSL package upstream that
        includes that inline function, but it appears to be unmaintained:
        https://github.com/Kitura/OpenSSL.git, which is another reason to swap out BlueSocket
        and BlueSSLService (which uses the Kitura OpenSSL package) in use here (future plans).

        Final workaround: define a local constant: pemErrorNoStartLine

        "I don't like it any more than you men." [bonus points if you get the reference]
        */
        let pemErrorNoStartLine = 151584876
        if socketError.errorCode == pemErrorNoStartLine {
            onUnrecoverableError?(socketError)
        } else {
            onError?(socketError)
            acceptClientConnection(on: connectionListenerSocket)
        }
    }

    private func acceptClientConnection(on socketConnection: Socket) {
        do {
            repeat {
                let newConnectionSocket = try socketConnection.acceptClientConnection()
                onAccept?(newConnectionSocket.remoteHostname)
                addNew(socketConnection: newConnectionSocket)
            } while continueRunning
        } catch {
            onError?(error)
            acceptClientConnection(on: socketConnection)
        }
    }

    private func addNew(socketConnection: Socket) {
        let connection = Connection(socket: socketConnection)
        _ = socketLockQueue.sync { [unowned self, connection] in
            self.activeConnections.insert(connection)
        }

        func runLoop() {
            do {
                switch connectionType {
                case .single(let responseOnConnect):
                    try processSingle(connection, responseOnConnect: responseOnConnect)
                case .persistent(let responseOnConnect, let responseForInput, let unexpectedClose):
                    try processPersistent(connection, responseOnConnect: responseOnConnect, responseForInput: responseForInput, unexpectedClose: unexpectedClose)
                }
            } catch let error as Socket.Error {
                onError?(error)
                switch connectionType {
                case .single:
                    remove(connection)
                case .persistent:
                    if connection.isPendingClose {
                        remove(connection)
                    } else {
                        runLoop()
                    }
                }
            } catch {
                let errorMessage = "Unexpected error: \(error.localizedDescription)"
                onError?(error)
                _ = try? socketConnection.write(from: errorMessage)
            }
        }

        let queue = DispatchQueue.global(qos: .default)
        queue.async {
           runLoop()
        }
    }

    private func processSingle(_ connection: Connection, responseOnConnect: ResponseOnConnect) throws {
        let socketConnection = connection.socket

        var requestData = Data(capacity: self.bufferSize)

        try socketConnection.read(into: &requestData)
        let requestString = String(data: requestData, encoding: .utf8)

        let certificate = certifiedConnections[connection.socket]
        let response = responseOnConnect(requestString, connection, certificate)
        try socketConnection.write(from: response.text)
        if let binary = response.binary {
            try socketConnection.write(from: binary)
        }

        socketConnection.close()
        remove(connection)
    }

    private func processPersistent(
                    _ connection: Connection,
                    responseOnConnect: ResponseOnConnect,
                    responseForInput: ResponseForInput,
                    unexpectedClose: UnexpectedClose
    ) throws {
        // TODO: Replace all references to `user` in this file with `request`
        var shouldKeepRunning = true

        let socketConnection = connection.socket

        let certificate = certifiedConnections[connection.socket]
        // TODO: Investigate if there is actually a "request string" here, even on first connect like above in `processSingle`
        let welcomeText = responseOnConnect(nil, connection, certificate)
        try socketConnection.write(from: welcomeText.text)

        var userInputData = Data(capacity: self.bufferSize)

        repeat {
            try socketConnection.setReadTimeout(value: UInt(1_500))
            let userInputBytes = try socketConnection.read(into: &userInputData)
            if userInputBytes > 0 || userInputData.isEmpty {
                guard let userInputString = String(data: userInputData, encoding: .utf8) else {
                    userInputData.count = 0
                    continue
                }

                let sanitizedUserInputString = userInputString.replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\r", with: "")
                guard !sanitizedUserInputString.isEmpty else {
                    if socketConnection.remoteConnectionClosed {
                        shouldKeepRunning = false
                        connection.isPendingClose = true
                        unexpectedClose(connection)
                        remove(connection)
                        break
                    } else {
                        continue
                    }
                }
                onUserInput?(socketConnection.remoteHostname, sanitizedUserInputString)
                do {
                    let output = responseForInput(sanitizedUserInputString, connection)
                    try socketConnection.write(from: output)
                    if connection.isPendingClose {
                        socketConnection.close()
                        remove(connection)
                        shouldKeepRunning = false
                        break
                    }
                }

                userInputData.count = 0
            }
        } while shouldKeepRunning
    }

    private func remove(_ connection: Connection) {
        if !connection.socket.isConnected {
            onClose?(connection.socket.remoteHostname)
            connection.close()
            socketLockQueue.sync { [unowned self] in
                activeConnections.remove(connection)
                certifiedConnections[connection.socket] = nil
            }
        }
    }
}
