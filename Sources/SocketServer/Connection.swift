//
//  Connection.swift
//  SocketServer
//
//  Created by Justin Marshall on 3/30/20.
//  Copyright © 2020 Justin Marshall. All rights reserved.
//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import Socket

public class Connection: Hashable, Encodable {
    private enum CodingKeys: String, CodingKey {
        case hostname
    }

    public var id: Int32 {
        return socket.socketfd
    }

    public var hostname: String { socket.remoteHostname }

    internal let socket: Socket
    internal var isPendingClose = false
    private let uuid = UUID()

    internal init(socket: Socket) {
        self.socket = socket
    }

    public static func ==(lhs: Connection, rhs: Connection) -> Bool {
        lhs.uuid == rhs.uuid
    }

    public func hash(into hasher: inout Hasher) {
        uuid.hash(into: &hasher)
    }

    public func close() {
        if isPendingClose {
            socket.close()
        } else {
            isPendingClose = true
        }
    }

    public func send(_ text: String) {
        _ = try? socket.write(from: text)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(socket.remoteHostname, forKey: .hostname)
    }
}
