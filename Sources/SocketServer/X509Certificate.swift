//
//  X509Certificate.swift
//  SocketServer
//
//  Created by Justin Marshall on 9/12/22.
//  Copyright © 2022 Justin Marshall. All rights reserved.
//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

import Foundation
import OpenSSL

/// This is a simple wrapper around some of OpenSSL's most basic X509 struct functionality.
/// Using this with anything other than `.pem` files is not officially supported, simply
/// because that has not been needed and therefore not tested.
public final class X509Certificate: Equatable {
    /// Represent's the encryption algorithm used to calculate this certificate's fingerprint
    public enum EncryptionAlgorithm {
        /// The SHA256 algorithm
        case sha256
    }

    /// An error representing something that goes wrong when writing an `X509Certificate` to disk
    /// via the `write(to:)` API.
    public enum WritingError: LocalizedError {
        /// Thrown when the underlying Swift Foundation API throws a directory creation error
        case foundationFileError(Error)

        /// Thrown when the underlying C `fopen` API fails
        case cFileError(errno: Int32)

        /// A human-readable description of the error
        public var errorDescription: String? {
            switch self {
            case .foundationFileError(let error):
                return "The underlying Foundation file API has returned: \(error.localizedDescription)"
            case .cFileError(let errno):
                return "The underlying C fopen API has returned errno: \(errno)"
            }
        }
    }

    /// Represents the certificate's fingerprint
    public let fingerprint: String

    /// Represents the encryption algorithm used to calculate this certificate's fingerprint
    public let encryptionAlgorithm: EncryptionAlgorithm

    /// Represents the certificate's subject Common Name
    public let commonName: String

    /// Represents the date before which the certificate is not valid
    public let notValidBeforeDate: Date

    /// Represents the date after which the certificate is not valid
    public let notValidAfterDate: Date

    /// Stores a reference to OpenSSL's X509 struct
    private let cPeerCertificate: OpaquePointer

    deinit {
        X509_free(cPeerCertificate)
    }

    /// Attempts to initialize an `X509Certificate` from the file at the given `URL`. The `URL`
    /// is expected to be in `.pem` format.
    ///
    /// - Parameter url: `URL` to the `.pem` file that you want to use. For consistency with
    ///                  this class's documented use case, it should have an extension of `.pem`,
    ///                  though this API does not enforce that.
    /// - Returns: An `X509Certificate` instance if: the program has read ("r") access to the `URL`,
    ///            OpenSSL can turn the file at the `URL` into an X509 struct reference and this
    ///            initializer is able to determine its 'common name', 'not before', and 'not after'
    ///            dates; `nil` in all other cases.
    public convenience init?(from url: URL) {
        guard let file = fopen(url.relativePath, "r") else {
            return nil
        }

        defer { fclose(file) }
        guard let peerCertificate = PEM_read_X509(file, nil, nil, nil) else {
            return nil
        }

        self.init(peerCertificate: peerCertificate)
    }

    /// Attempts to initialize an `X509Certificate` from an `OpaquePointer` containing an
    /// `SSL` struct from OpenSSL.
    ///
    /// - Parameter cSSL: An `OpaquePointer` containing an `SSL` struct from OpenSSL
    /// - Returns: An `X509Certificate` instance if OpenSSL can turn the `cSSL` parameter
    ///            into an X509 struct reference and this initializer is able to determine
    ///            its 'common name', 'not before', and 'not after' dates; `nil` otherwise.
    internal convenience init?(from cSSL: OpaquePointer?) {
        guard let peerCertificate = SSL_get_peer_certificate(cSSL) else {
            return nil
        }

        self.init(peerCertificate: peerCertificate)
    }

    private init?(peerCertificate: OpaquePointer) {
        guard let commonName = Self.extractCommonName(from: peerCertificate) else {
            return nil
        }

        self.commonName = commonName

        var tmpTime = tm()
        let time = withUnsafeMutablePointer(to: &tmpTime) { $0 }

        let notValidBeforeDate = X509_get0_notBefore(peerCertificate)
        ASN1_TIME_to_tm(notValidBeforeDate, time)
        let notValidBeforeDateComponents = DateComponents(tm: time.pointee)

        let notValidAfterDate = X509_get0_notAfter(peerCertificate)
        ASN1_TIME_to_tm(notValidAfterDate, time)
        let notValidAfterDateComponents = DateComponents(tm: time.pointee)

        guard let notValidBeforeDate = notValidBeforeDateComponents.date,
            let notValidAfterDate = notValidAfterDateComponents.date else {
                return nil
        }

        self.notValidBeforeDate = notValidBeforeDate
        self.notValidAfterDate = notValidAfterDate

        let chars = [CUnsignedChar](repeating: 0, count: Int(EVP_MAX_MD_SIZE))
        let md = UnsafeMutablePointer<UInt8>.allocate(capacity: chars.count)

        let algorithmToUse: EncryptionAlgorithm = .sha256
        switch algorithmToUse {
        case .sha256:
            X509_digest(peerCertificate, EVP_sha256(), md, nil)
        }

        // The `algorithmToUse` variable and the switch statement above should serve as a
        // compile-time reminder to update both the `encryptionAlgorithm` property and the
        // algorithm passed into `OpenSSL`, in the event that the encryption algorithm
        // changes. This should improve maintainability.
        self.encryptionAlgorithm = algorithmToUse

        let fingerprint = (0..<32)
            .map { pos in
                String(format: "%02x", arguments: [md[pos]])
            }
            .reduce("") { $0 + $1.uppercased() + ":" }
            .dropLast()
        self.fingerprint = String(fingerprint)
        self.cPeerCertificate = peerCertificate
    }

    private static func extractCommonName(from peerCertificate: OpaquePointer) -> String? {
        let x509Name = X509_get_subject_name(peerCertificate)

        // Docs @ https://www.openssl.org/docs/man1.1.1/man3/X509_NAME_get_index_by_NID.html
        var lastPosition: Int32 = -1 // always start with -1, per docs
        lastPosition = X509_NAME_get_index_by_NID(x509Name, NID_commonName, lastPosition)
        let notFound = -1 // per docs
        guard lastPosition != notFound else { return nil }

        let x509NameEntry = X509_NAME_get_entry(x509Name, lastPosition)
        guard let nameASN1String = X509_NAME_ENTRY_get_data(x509NameEntry) else {
            return nil
        }

        // Per RFC 5280 (https://www.rfc-editor.org/rfc/rfc5280): ub-common-name-length INTEGER ::= 64
        let maxCommonNameLength = 64
        var utf8String: UnsafeMutablePointer<UInt8>? = UnsafeMutablePointer<UInt8>.allocate(capacity: maxCommonNameLength)

        // Docs @ https://www.openssl.org/docs/man1.1.1/man3/ASN1_STRING_to_UTF8.html
        // The length of out [the first parameter] is returned or a negative error code.
        // The buffer *out [the first parameter] should be freed using OPENSSL_free()
        //
        // OPENSSL_free doesn't seem to be available in this Swift package? Looking at
        // the openssl/crypto.h file where that macro is defined, it looks like it just
        // calls off to CRYPTO_free. Interestingly, I can see both OPENSSL_free and
        // CRYPTO_free in the file when going to the declaration for CRYPTO_free.
        // In the interest of not doing something I don't understand, I'm just not going
        // to call any `*free` function.
        let thisCommonNameLength = ASN1_STRING_to_UTF8(&utf8String, nameASN1String)
        guard let utf8String = utf8String,
            thisCommonNameLength > 0 else {
                return nil
            }

        var commonNameCharacters: [String] = []
        for i in (0..<thisCommonNameLength) {
            let character = utf8String[Int(i)]
            let codeUnit = Unicode.UTF8.CodeUnit(character)
            let stringValue = String(decoding: [codeUnit], as: UTF8.self)
            let cNullCharacter = "\0"
            guard stringValue != cNullCharacter else {
                break
            }
            commonNameCharacters.append(stringValue)
        }

        let commonName = commonNameCharacters.joined()
        return commonName
    }

    /// Attempts to write the `X509Certificate` to disk, if the program has write ("w") access
    /// to the `URL`. If for any reason the file cannot be created, an error will be thrown.
    ///
    /// - Parameter url: `URL` to which the file will be written. For consistency with this
    ///                  class's documented use case, it should have an extension of `.pem`,
    ///                  though this API does not enforce that.
    /// - Returns: Nothing, if no errors encountered
    /// - Throws: An instance of `WritingError`, describing the failed write
    public func write(to url: URL) throws {
        var mutableURL = url
        mutableURL.deleteLastPathComponent()
        if !FileManager.default.fileExists(atPath: mutableURL.path) {
            do {
                try FileManager.default.createDirectory(at: mutableURL,
                                                        withIntermediateDirectories: false)
            } catch {
                throw WritingError.foundationFileError(error)
            }
        }
        guard let file = fopen(url.relativePath, "w") else { throw WritingError.cFileError(errno: errno) }
        PEM_write_X509(file, cPeerCertificate)
        fclose(file)
    }

    /// Compares two `X509Certificate` instances for equality. The following properties are
    /// checked: `fingerprint`, `notValidBeforeDate`, and `notValidAfterDate`.
    ///
    /// - Parameter lhs: `X509Certificate` to compare against `rhs`
    /// - Parameter rhs: `X509Certificate` to compare against `lhs`
    /// - Returns: A `Bool` indicating equality or not.
    public static func ==(lhs: X509Certificate, rhs: X509Certificate) -> Bool {
        lhs.fingerprint == rhs.fingerprint &&
        lhs.encryptionAlgorithm == rhs.encryptionAlgorithm &&
        lhs.commonName == rhs.commonName &&
        lhs.notValidBeforeDate == rhs.notValidBeforeDate &&
        lhs.notValidAfterDate == rhs.notValidAfterDate
    }
}

private extension DateComponents {
    /// Initializes a `DateComponents` from the given `tm`.
    ///
    /// - Parameter tm: An instance of `tm` that will be converted
    /// - Returns: A `DateComponents` instance
    init(tm: tm) {
        let yearOffsetForCAPI = 1900

        self.init(
            calendar: .autoupdatingCurrent,
            timeZone: TimeZone(abbreviation: "GMT"),
            year: Int(tm.tm_year) + yearOffsetForCAPI,
            month: Int(tm.tm_mon) + 1,
            day: Int(tm.tm_mday),
            hour: Int(tm.tm_hour),
            minute: Int(tm.tm_min),
            second: Int(tm.tm_sec)
        )
    }
}
