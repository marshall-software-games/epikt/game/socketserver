//
//  SocketServer.h
//  SocketServer
//
//  Created by Justin Marshall on 6/10/19.
//  Copyright © 2019 Justin Marshall. All rights reserved.
//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SocketServer.
FOUNDATION_EXPORT double SocketServerVersionNumber;

//! Project version string for SocketServer.
FOUNDATION_EXPORT const unsigned char SocketServerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SocketServer/PublicHeader.h>


