//
//  This file is part of SocketServer.
//
//  SocketServer is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License only.
//
//  SocketServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with SocketServer. If not, see <https://www.gnu.org/licenses/>.
//

@testable import SocketServer
import XCTest

final class SocketServerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssert(true, "example")
    }

    static var allTests = [
        ("testExample", testExample)
    ]
}
